---
layout: post
title: "July 9, 2019: router fail"
publishDate: "2019-11-26"
modifiedDate: false
seoDescription: "When my router failed, the raspberry pi came to my rescue."
exclude: false
---

I arrived home after a relaxing vacation in Taiwan.  Start up laptop... *no network connectivity*. 

*"That's weird"*

check router? *just a flashing red dot...*

connect directly via ethernet? *no dhcp!* 

static ip? *still nothing!*

Reset to factory defaults by finding a toothpick and that tiny button-hole using a multitude of ways recommended by random sources on the internet? *no.*

Conclusion: *it's dead* :(

Yet another reason why proprietary hardware is an engineering fallacy.

Luckily, I had a raspberry pi and figured since it has wifi and ethernet, isn't it straightforward to get it to be a router?

Tether laptop through phone to figure out simple connectivity... the answer is *yes!*  Considering my situation, I was very satisfied.

Since then I have found this functionality to be very useful...  But the process to configure details has been quite painful.  I figured I could have a project focused
on saving time for other people with similar needs.  The core purpose would be to provide a binary image of a working raspberry pi wifi access point.  
This by itself would be very useful for a variety of reasons while overall cost is zero, or just the storage space of an 8 GB USB drive.

From the base image, as a functional system, the only bounds is the imagination...
