---
layout: page
title: Quickstart
permalink: /quickstart
---

### Download

## Version Alpha
ubuntu-mate hotspot berry.bluetree.jp with pw: 83ry8lu3
If anything, this is a proof-of-concept.  It is simply the mate distro with a hotspot configuration built in.

[alpha ubuntu mate 18.04.2-beta1 desktop arm64 torrent](./ubuntu-mate-18.04.2-beta1-desktop-arm64+raspi3-ext4.img.zip.torrent )

### Install

#### Linux:

```
ddrescue --force ./berry-blue-tree-core.img /dev/sdxxx
```

#### GUI-based:

[etcher](https://www.balena.io/etcher/) has worked well.

### Combine

-   Attach to usb before powering on raspberry pi.
-   Check for ```berry.bluetree.jp``` SSID
-   login with the default password

### Questions?

Please use this website's [Issue Tracker](https://gitlab.com/aokinobu/berry.bluetree.jp/issues) to raise issues.

