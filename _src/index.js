// Use the window load event to keep the page load performant
window.addEventListener('load', () => {
  navigator.serviceWorker.register('/berry-worker.js');
});
